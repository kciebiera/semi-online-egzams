#include "opt.h"

#include <glpk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static bool check_results(const int size, egzlp_preference * preferences,
			  glp_prob * lp)
{
	bool ok = true;
	int r = 0;
	for (int i = 1; i <= size; i++) {
		double v = glp_get_col_prim(lp, i);
		if (v != 0 && v != 1) {
			ok = false;
		}
		if (v == 1)
			r++;
		preferences[i - 1].accept = v;
	}
	printf("OK: %d (%d)\n", ok, r);
	return ok;
}

static void display_results(const int size, glp_prob * lp, bool details)
{
	double z = glp_get_obj_val(lp);
	if (details) {
		for (int i = 1; i <= size; i++)
			printf("%d = %g ", i, glp_get_col_prim(lp, i));
		printf("\n");
	}
	printf("z = %f\n", z);
}

void egzlp_solve(egzlp_problem * problem)
{
	const int size = problem->size;
	egzlp_preference *preferences = problem->preferences;
	const int students = problem->students;
	bool *accepted = problem->accepted;
	const int slots = problem->slots;
	const int *seats = problem->seats;
	glp_prob *lp = glp_create_prob();
	glp_smcp parm;
	glp_init_smcp(&parm);
	glp_set_obj_dir(lp, GLP_MAX);
	glp_add_cols(lp, size);
	for (int i = 1; i <= size; i++) {
		glp_set_col_bnds(lp, i, GLP_DB, 0, 1);
		glp_set_obj_coef(lp, i, preferences[i - 1].value);
	}

	for (int student = 0; student <= students; student++) {
		int ind[1 + size];
		double val[1 + size];
		int len = 0;
		for (int i = 0; i < size; i++) {
			if (preferences[i].student == student) {
				++len;
				ind[len] = i + 1;
				val[len] = 1;
			}
		}
		if (len > 0) {
			int row = glp_add_rows(lp, 1);
			glp_set_mat_row(lp, row, len, ind, val);
			if (accepted[student])
				glp_set_row_bnds(lp, row, GLP_FX, 1, 1);
			else
				glp_set_row_bnds(lp, row, GLP_DB, 0, 1);
		}
	}

	for (int slot = 0; slot <= slots; slot++) {
		int ind[1 + size];
		double val[1 + size];
		int len = 0;
		for (int i = 0; i < size; i++) {
			if (preferences[i].slot == slot) {
				++len;
				ind[len] = i + 1;
				val[len] = 1;
			}
		}
		if (len > 0) {
			int row = glp_add_rows(lp, 1);
			glp_set_mat_row(lp, row, len, ind, val);
			glp_set_row_bnds(lp, row, GLP_DB, 0, seats[slot]);
		}
	}
	glp_simplex(lp, &parm);
	if (!check_results(size, problem->preferences, lp)) {
		glp_intopt(lp, NULL);
		check_results(size, problem->preferences, lp);
	}

	display_results(size, lp, false);

	glp_delete_prob(lp);
	glp_free_env();
}
