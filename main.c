#include "opt.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

const int students_count = 1000;
const int slots_count = 100;
const int seats_per_slot = 10;

egzlp_problem *problem;

bool pow_of_2(unsigned int i)
{
	return (i != 0) && !(i & (i - 1));
}

void generate_full_preferences()
{
	problem = malloc(sizeof(egzlp_problem));
	problem->size = 0;
	problem->students = slots_count * seats_per_slot;
	problem->slots = slots_count;
	problem->seats = calloc(slots_count, sizeof(int));
	for (int i = 0; i < slots_count; i++)
		problem->seats[i] = seats_per_slot;
	problem->preferences = malloc(sizeof(egzlp_preference));
	problem->accepted = calloc(students_count, sizeof(bool));
	int single_preference[slots_count];
	for (int i = 0; i < slots_count; i++)
		single_preference[i] = i;
	for (int student = 0; student < slots_count * seats_per_slot; student++) {
		int student_preference = student / seats_per_slot + 1;
		for (int i = 0; i < student_preference; i++) {
			if (pow_of_2(problem->size))
				problem->preferences =
				    realloc(problem->preferences,
					    2 * problem->size *
					    sizeof(egzlp_preference));
			problem->preferences[problem->size].student = student;
			problem->preferences[problem->size].slot =
			    single_preference[i];
			problem->preferences[problem->size].value =
			    1000000 + i + 1;
			problem->size++;
		}
	}
}

void generate_random_preferences()
{
	problem = malloc(sizeof(egzlp_problem));
	problem->size = 0;
	problem->students = students_count;
	problem->slots = slots_count;
	problem->seats = calloc(slots_count, sizeof(int));
	for (int i = 0; i < slots_count; i++)
		problem->seats[i] = seats_per_slot;
	problem->preferences = malloc(sizeof(egzlp_preference));
	problem->accepted = calloc(students_count, sizeof(bool));
	for (int student = 0; student < students_count; student++) {
		int student_preference = rand() % slots_count + 1;
		int single_preference[slots_count];
		for (int i = 0; i < student_preference; i++) {
			int c = rand() % (i + 1);
			if (c != i)
				single_preference[i] = single_preference[c];
			single_preference[c] = i;
		}
		for (int i = 0; i < student_preference; i++) {
			if (pow_of_2(problem->size))
				problem->preferences =
				    realloc(problem->preferences,
					    2 * problem->size *
					    sizeof(egzlp_preference));
			problem->preferences[problem->size].student = student;
			problem->preferences[problem->size].slot =
			    single_preference[i];
			problem->preferences[problem->size].value =
			    1000000 + i + 1;
			problem->size++;
		}
	}
}

void generate_preferences_acc()
{
	problem = malloc(sizeof(egzlp_problem));
	problem->size = 0;
	problem->students = 12;
	problem->slots = 2;
	problem->seats = calloc(2, sizeof(int));
	problem->seats[0] = 3;
	problem->seats[1] = 3;
	problem->preferences = calloc(12 * 2, sizeof(egzlp_preference));
	problem->accepted = calloc(12, sizeof(bool));
	for (int student = 0; student < 6; student++)
		problem->accepted[student] = true;
	for (int student = 0; student < 12; student++) {
		for (int i = 0; i < 2; i++) {
			problem->preferences[problem->size].student = student;
			problem->preferences[problem->size].slot = i;
			problem->preferences[problem->size].value =
			    1000000 + i + 1;
			problem->size++;
		}
	}
}

void display_preferences()
{
	for (int i = 0; i < problem->size; i++)
		printf("%d: student %d slot %d -> %d (%d)\n", i + 1,
		       problem->preferences[i].student,
		       problem->preferences[i].slot,
		       problem->preferences[i].accept,
		       problem->preferences[i].value);
	printf("Total %d preferences\n", problem->size);
}

void free_problem()
{
	free(problem->seats);
	free(problem->preferences);
	free(problem->accepted);
	free(problem);
}

int main()
{
	srand(10);
	generate_preferences_acc();
	egzlp_solve(problem);
	display_preferences();
	free_problem();
	generate_full_preferences();
	egzlp_solve(problem);
	free_problem();

	generate_random_preferences();
	egzlp_solve(problem);
	free_problem();

	return 0;
}
