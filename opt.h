#ifndef OPT_H
#define OPT_H
#include <stdbool.h>

typedef struct {
	int student;
	int slot;
	int value;
	bool accept;
} egzlp_preference;

typedef struct {
	int size;
	egzlp_preference *preferences;
	int students;
	bool *accepted;
	int slots;
	int *seats;
} egzlp_problem;

void egzlp_solve(egzlp_problem * problem);

#endif
